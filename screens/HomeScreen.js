import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions,
  Image,
  processColor,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AutoHeightImage from 'react-native-auto-height-image';
import {CombinedChart, BarChart, PieChart} from 'react-native-charts-wrapper';
import Icon from 'react-native-vector-icons/FontAwesome';

const windowHeight = Dimensions.get("window").height;
const windowWidth = Dimensions.get("window").width;

const avatarImg = require('../assets/image/avatar.png');

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      data: {
        dataSets: [{
          values: [{y: 100}, {y: 105}, {y: 102}, {y: 110}, {y: 114}, {y: 109}, {y: 105}],
          label: '',
          config: {
            color: processColor('#a1c0ff'),
            barShadowColor: processColor('lightgrey'),
            highlightAlpha: 90,
            highlightColor: processColor('red'),
          }
        }],

        config: {
          barWidth: 0.35,
        }
      },
      xAxis: {
        valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
        granularityEnabled: true,
        granularity : 1,
        drawGridLines: false,
        position: 'BOTTOM',
      },
      pieChartLegend: {
        enabled: true,
        textSize: 15,
        form: 'CIRCLE',

        horizontalAlignment: "RIGHT",
        verticalAlignment: "CENTER",
        orientation: "VERTICAL",
        wordWrapEnabled: true
      },
      pieChartData: {
        dataSets: [{
          values: [{value: 45, label: 'Nhà ở'},
            {value: 21, label: 'Ăn uống'},
            {value: 15, label: 'Mua sắm'},
            {value: 9, label: 'Học tập'},
            {value: 15, label: 'Gia đình'}],
          label: '',
          config: {
            colors: [processColor('#C0FF8C'), processColor('#FFF78C'), processColor('#FFD08C'), processColor('#8CEAFF'), processColor('#FF8C9D')],
            valueTextSize: 10,
            valueTextColor: processColor('green'),
            sliceSpace: 5,
            selectionShift: 13,
            // xValuePosition: "OUTSIDE_SLICE",
            // yValuePosition: "OUTSIDE_SLICE",
            valueFormatter: "#.#'%'",
            valueLineColor: processColor('green'),
            valueLinePart1Length: 0.5
          }
        }],
      },
      pieChartDescription: {
        text: '',
        textSize: 15,
        textColor: processColor('darkgray'),

      }
    }

    this.navigate = this.navigate.bind(this);
  }

  componentDidMount(){
    this.setState({...this.state, highlights: [{x: 1, y:150, dataIndex: 4}, {x: 2, y:106, dataIndex: 4}]});
  }


  navigate(index){
    console.log(index);
    if(index === 0){
      this.props.navigation.navigate("Add");
    }
    else if(index == 2){
      this.props.navigation.navigate("History");
    }
  }

  render(){    
    return(
      <View style={{
        flex: 1,
        zIndex: 0,
      }}>
        <View style={{
          flex: 1,
          backgroundColor: '#f8edeb'
        }}>
          <View style={{
            flex: 2,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
            <View style={{
              flex: 4,

              paddingLeft: windowWidth/20,
            }}>
              <Text style={{
                fontSize: windowHeight/25,
                fontWeight: 'bold',                
              }}>
                Thống kê
              </Text>
            </View>            
            <View style={{
              flex: 1,

            }}>
              <AutoHeightImage
                width={windowHeight/16}
                source={avatarImg}
                style={{borderRadius: windowHeight/56,}}
              />
            </View>
          </View>
          <View style={{
            flex: 10,         
            marginBottom: windowHeight/7,
            marginLeft: windowWidth/25,
            borderRadius: windowWidth/25,
          }}>
            <View style={{
              flex: 3,
              flexDirection: 'column',
              backgroundColor: '#caf0f8',
              borderTopLeftRadius: 30,
              borderBottomLeftRadius: 30,
              paddingLeft: windowWidth/10,
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 12,
              },
              shadowOpacity: 0.58,
              shadowRadius: 16.00,
              elevation: 24,
            }}>
              <View style={{
                marginTop: 10,
                marginLeft: 5,
              }}>
                <Text style={{
                  fontSize: 18,
                }}>
                  Số dư trong ví
                </Text>
              </View>
              <View style={{
                paddingBottom: 5,
              }}>
                <Text style={{
                  fontSize: 30,
                  fontWeight: 'bold',
                }}>
                  $ 75.000
                </Text>
              </View>
              <View style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
                <View style={{
                  flex: 6,
                  flexDirection: 'row',
                }}>
                  <View style={{
                    backgroundColor: '#ff0000',
                    height: 10,
                    borderColor: '#000000',
                    borderWidth: 1,
                    flex: 7,
                    borderTopLeftRadius: 5,
                    borderBottomLeftRadius: 5,
                  }}>
                    
                  </View>
                  <View style={{
                    backgroundColor: '#ffffff',
                    height: 10,
                    borderColor: '#000000',
                    borderWidth: 1,
                    flex: 3,
                    borderTopRightRadius: 5,
                    borderBottomRightRadius: 5,
                  }}>
                    
                  </View>
                </View>
                <View style={{
                  flex: 1,
                  marginLeft: 10,
                  justifyContent: 'center',
                }}>
                  <Text>
                    72%
                  </Text>
                </View>
              </View>
              
            </View>
            <View style={{
              flex: 4,
              backgroundColor: '#fcf6bd',
              marginLeft: -20,
              marginRight: 20,
              marginTop: 20,
              paddingBottom: 10,
              borderTopRightRadius: 30,
              borderBottomRightRadius: 30,
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 12,
              },
              shadowOpacity: 0.58,
              shadowRadius: 16.00,
              elevation: 24,
            }}>
              <BarChart
                style={{flex: 1, marginTop: 10}}
                data={this.state.data}
                xAxis={this.state.xAxis}
                // animation={{durationX: 3000}}
                gridBackgroundColor={processColor('#ffffff')}
                visibleRange={{x: { min: 7, max: 7 }}}
                drawBarShadow={false}
                drawValueAboveBar={true}
                drawHighlightArrow={true}
                // onChange={(event) => console.log(event.nativeEvent)}
                chartDescription={{text: ''}}
                legend={{ enabled: false }}
                yAxis={{drawAxisLines: false,
                  left: { enabled: false, drawGridLines: false, granularityEnabled: true, granularity: 1 },
                  right: { enabled: false, drawGridLines: false, granularityEnabled: true, granularity: 1}}}

              />

            </View>
            <View style={{
              flex: 5, 
              flexDirection: 'row',
              backgroundColor: '#eff7f6',
              borderTopLeftRadius: 30,
              borderBottomLeftRadius: 30,
              marginTop: 20,
              paddingLeft: 0,
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 12,
              },
              shadowOpacity: 0.58,
              shadowRadius: 16.00,
              elevation: 24,             
            }}>
              <View style={{
                flex: 12,
              }}>
                <PieChart
                  style={{flex: 1}}
                  logEnabled={true}
                  // chartBackgroundColor={processColor('pink')}
                  chartDescription={this.state.pieChartDescription}
                  data={this.state.pieChartData}
                  legend={this.state.pieChartLegend}

                  entryLabelColor={processColor('green')}
                  entryLabelTextSize={7}
                  drawEntryLabels={false}

                  rotationEnabled={true}
                  rotationAngle={45}
                  usePercentValues={false}
                  // styledCenterText={{text:'Pie center text!', color: processColor('pink'), size: 5}}
                  centerTextRadiusPercent={100}
                  holeRadius={40}
                  holeColor={processColor('#f0f0f0')}
                  transparentCircleRadius={45}
                  transparentCircleColor={processColor('#f0f0f088')}
                  maxAngle={360}
                  // onSelect={this.handleSelect.bind(this)}
                  // onChange={(event) => console.log(event.nativeEvent)}
                />
              </View>
              <View style={{
                flex: 1,
              }}>
                
              </View>
            </View>
            
          </View>
        </View>
      </View>
    )
  }
}

export default HomeScreen;
