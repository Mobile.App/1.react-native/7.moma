import React, { Component } from 'react';
import { View, Text, Dimensions, TextInput, Button, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AutoHeightImage from 'react-native-auto-height-image';
import DateTimePickerModal from "react-native-modal-datetime-picker";

const windowHeight = Dimensions.get("window").height;
const windowWidth = Dimensions.get("window").width;

const avatarImg = require('../assets/image/avatar.png');

class AddScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datePickerVisible: false,
      dateSelect: '',
    };
  }

  componentDidMount(){
    let date = new Date().getDate();
    let month = new Date().getMonth() + 1;
    let year = new Date().getFullYear();
    let str = date + '/' + month + '/' + year;
    this.setState({dateSelect: str});
  }

  hideDatePicker = () => {
    this.setState({datePickerVisible: false});
  };
  openDataPicker = () => {
    this.setState({datePickerVisible: true});
  };
 
  handleConfirm = (dateData) => {
    let date = dateData.getDate();
    let month = dateData.getMonth() + 1;
    let year = dateData.getFullYear();
    let str = date + '/' + month + '/' + year;
    this.state.dateSelect = str;
    this.hideDatePicker();
  };

  render() {
    return (
      <View style={{
        flex: 1,
        zIndex: 0,
      }}>
        <View style={{
          flex: 1,
          backgroundColor: '#cff9fd'
        }}>
          <View style={{
            flex: 2,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
            <View style={{
              flex: 4,

              paddingLeft: windowWidth/20,
            }}>
              <Text style={{
                fontSize: windowHeight/25,
                fontWeight: 'bold',
                color: '#000000',
              }}>
                Ghi sổ
              </Text>
            </View>            
            <View style={{
              flex: 1,

            }}>
              <AutoHeightImage
                width={windowHeight/16}
                source={avatarImg}
                style={{borderRadius: windowHeight/56,}}
              />
            </View>
          </View>

          <View style={{
            flex: 6.5,
            flexDirection: 'column',            
            marginLeft: windowWidth/35,
            borderTopLeftRadius: windowWidth/10,
            borderBottomLeftRadius: windowWidth/20,
            backgroundColor: '#9ef3fb',
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 12,
            },
            shadowOpacity: 0.58,
            shadowRadius: 16.00,

            elevation: 24,
          }}>
            <View style={{
              flex: 5,
              marginTop: 20,
              marginBottom: 50,
              marginLeft: 18,
              paddingLeft: 10,
              //backgroundColor: '#fcf6bd',
              borderTopLeftRadius: 40,
              borderBottomLeftRadius: 40,
            }}>
              <View style={{
                flex: 1,
                flexDirection: 'row',
                marginLeft: 10,
                marginRight: 10,
                marginTop: 20,
                marginBottom: 0,
                alignItems: 'center',
              }}>
                <View style={{
                  flex: 2,
                  //backgroundColor: '#dddd44',
                  justifyContent: 'center',
                }}>
                  <Text style={{
                    fontSize: 20,
                    fontWeight: 'bold',
                    color: '#000000',
                  }}>
                    Số tiền
                  </Text>
                </View>
                <View style={{
                  flex: 3,
                  backgroundColor: '#98e6ed',
                  justifyContent: 'center',
                  shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 6,
                  },
                  shadowOpacity: 0.37,
                  shadowRadius: 7.49,
                  elevation: 12,
                }}>
                  <TextInput underlineColorAndroid ='transparent' keyboardType='numeric' 
                    style={{ 
                      flex: 1,
                      fontSize: 20, 
                      fontWeight: 'bold',
                      textAlign: 'center',
                      color: '#774c60',
                    }} 
                  />
                </View>
              </View>
              <View style={{
                flex: 1,
                flexDirection: 'row',
                marginLeft: 10,
                marginRight: 10,
                marginTop: 20,
                marginBottom: 0,
                alignItems: 'center',
                //backgroundColor: '#00ff00',
              }}>
                <View style={{
                  flex: 2,
                  justifyContent: 'center',
                }}>
                  <Text style={{
                    fontSize: 20,
                    fontWeight: 'bold',
                    color: '#000000',
                  }}>
                    Danh mục
                  </Text>
                </View>
                <TouchableOpacity onPress={this.openDataPicker} style={{
                  flex: 3,
                  backgroundColor: '#98e6ed',
                  justifyContent: 'center',
                  alignItems: 'center',
                  shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 6,
                  },
                  shadowOpacity: 0.37,
                  shadowRadius: 7.49,

                  elevation: 12,
                }}>
                  <Text style={{
                    fontSize: 20,
                    color: '#774c60',
                  }}>
                    Lương
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{
                flex: 1,
                flexDirection: 'row',
                marginLeft: 10,
                marginRight: 10,
                marginTop: 20,
                marginBottom: 0,
                alignItems: 'center',
                //backgroundColor: '#00ff00',
              }}>
                <View style={{
                  flex: 2,
                  justifyContent: 'center',
                }}>
                  <Text style={{
                    fontSize: 20,
                    fontWeight: 'bold',
                    color: '#000000',
                  }}>
                    Ngày
                  </Text>
                </View>
                <TouchableOpacity onPress={this.openDataPicker} style={{
                  flex: 3,
                  backgroundColor: '#98e6ed',
                  justifyContent: 'center',
                  alignItems: 'center',
                  shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 6,
                  },
                  shadowOpacity: 0.37,
                  shadowRadius: 7.49,

                  elevation: 12,
                }}>
                  <DateTimePickerModal
                    isVisible={this.state.datePickerVisible}
                    mode="date"
                    onConfirm={(date)=>this.handleConfirm(date)}
                    onCancel={()=>this.hideDatePicker()}
                  />
                  <Text style={{
                    fontSize: 20,
                    color: '#774c60',
                  }}>
                    {this.state.dateSelect}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{
                flex: 1,
                flexDirection: 'row',
                marginLeft: 10,
                marginRight: 10,
                marginTop: 20,
                marginBottom: 0,
                alignItems: 'center',
                //backgroundColor: '#00ff00',
              }}>
                <View style={{
                  flex: 2,
                  justifyContent: 'center',
                }}>
                  <Text style={{
                    fontSize: 20,
                    fontWeight: 'bold',
                    color: '#000000',
                  }}>
                    Định kỳ
                  </Text>
                </View>
                <TouchableOpacity onPress={this.openDataPicker} style={{
                  flex: 3,
                  backgroundColor: '#98e6ed',
                  justifyContent: 'center',
                  alignItems: 'center',
                  shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 6,
                  },
                  shadowOpacity: 0.37,
                  shadowRadius: 7.49,
                  elevation: 12,
                }}>
                  <Text style={{
                    fontSize: 20,
                    color: '#774c60',
                  }}>
                    Không chọn
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{
              flex: 0.8,
              flexDirection: 'row',
              // marginRight: 100,
            }}>
              <View style={{
                flex: 5,
                marginLeft: 15,
                backgroundColor: '#b5c9c3',
                borderRadius: windowWidth/10,                
                justifyContent: 'center',
                alignItems: 'center',
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: -5,
                },
                shadowOpacity: 0.34,
                shadowRadius: 6.27,

                elevation: 10,
              }}>
                <Text style={{
                  fontSize: 25,
                  fontWeight: 'bold',
                }}>Hủy</Text>
              </View>
              <View style={{
                flex: 1
              }}>

              </View>
              <View style={{
                flex: 5,
                marginRight: 15,
                backgroundColor: '#6bf178',                
                borderRadius: windowWidth/10,
                justifyContent: 'center',
                alignItems: 'center',
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 0,
                },
                shadowOpacity: 1.00,
                shadowRadius: 6.27,

                elevation: 10,
              }}>
                <Text style={{
                  fontSize: 25,
                  fontWeight: 'bold',
                }}>Hoàn tất</Text>
              </View>
            </View>
            <View style={{flex: 0.2}}></View>
          </View>
          <View style={{flex:3.5}}></View>
        </View>
      </View>
    );
  }
}

export default AddScreen;
