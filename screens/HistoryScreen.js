import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AutoHeightImage from 'react-native-auto-height-image';

const windowHeight = Dimensions.get("window").height;
const windowWidth = Dimensions.get("window").width;

const avatarImg = require('../assets/image/avatar.png');
class HistoryScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{
        flex: 1,
        zIndex: 0,
      }}>
        <View style={{
          flex: 1,
          backgroundColor: '#fff8d3'
        }}>
          <View style={{
            flex: 2,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
            <View style={{
              flex: 4,

              paddingLeft: windowWidth/20,
            }}>
              <Text style={{
                fontSize: windowHeight/25,
                fontWeight: 'bold',                
              }}>
                Lịch sử
              </Text>
            </View>            
            <View style={{
              flex: 1,

            }}>
              <AutoHeightImage
                width={windowHeight/16}
                source={avatarImg}
                style={{borderRadius: windowHeight/56,}}
              />
            </View>
          </View>
          <View style={{
            flex: 10,         
            marginBottom: windowHeight/7,
            marginLeft: windowWidth/25,
            borderRadius: windowWidth/25,
          }}>
            
          </View>
        </View>
      </View>
    );
  }
}

export default HistoryScreen;
