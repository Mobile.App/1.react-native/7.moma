import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/FontAwesome';

const windowHeight = Dimensions.get("window").height;
const windowWidth = Dimensions.get("window").width;

import HomeScreen from './screens/HomeScreen.js'
import AddScreen from './screens/AddScreen.js'
import HistoryScreen from './screens/HistoryScreen.js'


const Stack = createStackNavigator();
const navigationRef = React.createRef();

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screenCurrent: 1,
      addIconName: 'plus-circle',
      addIconSize: 70,
      addIconColor: '#577399',
      dashboardIconColor: '#ff686b',
      dashboardIconSize: 35,
      historyIconColor: '#577399',
      historyIconSize: 30,
    };
  }

  componentDidMount(){
    console.log(Stack.Navigator);
  }

  navigate(name, params) {
    console.log('dang chay');
    navigationRef.current && navigationRef.current.navigate(name, params);
  }

  gotoScreen(screenSelect){
    if(this.state.screenCurrent != screenSelect){
      this.state.screenCurrent = screenSelect;
      
      if(screenSelect == 0){
        this.navigate("Add", { userName: 'Lucy' });
        this.setState({
          //addIconName: 'check', 
          //addIconSize: 50, 
          addIconColor: '#ff70a6',
          dashboardIconColor: '#577399',
          dashboardIconSize: 30,
          historyIconColor: '#577399',
          historyIconSize: 30});
      }
      else if(screenSelect == 1){
        this.navigate("Home", null);
        this.setState({
          //addIconName: 'plus-circle', 
          //addIconSize: 70,
          addIconColor: '#577399',
          dashboardIconColor: '#ff686b',
          dashboardIconSize: 35,
          historyIconColor: '#577399',
          historyIconSize: 30});
      }
      else if(screenSelect == 2){
        this.navigate("History", null);
        this.setState({
          //addIconName: 'plus-circle', 
          //addIconSize: 70,
          addIconColor: '#577399',
          dashboardIconColor: '#577399',
          dashboardIconSize: 30,
          historyIconColor: '#ff686b',
          historyIconSize: 35});
      }
    }
  }

  render() {
    return (
      <View style={{flex:1}}>
        <NavigationContainer ref={navigationRef}>
          <Stack.Navigator screenOptions={{
            headerShown: false
          }}>
            <Stack.Screen
              name="Home"
              component={HomeScreen}
            />
            <Stack.Screen name="Add" component={AddScreen} />
            <Stack.Screen name="History" component={HistoryScreen} />
          </Stack.Navigator>
        </NavigationContainer>
        <View style={{ 
          position: 'absolute', 
          backgroundColor: '#1c2541', 
          bottom: 0, 
          zIndex: 1, 
          width: '100%', 
          height: windowHeight/12, 
          flexDirection: 'row', 
          flex: 1,
        }}>
          <TouchableOpacity onPress={()=>this.gotoScreen(1)} style={{
            //backgroundColor: '#ff0000',
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            <Icon
              name='line-chart'
              color={this.state.dashboardIconColor}              
              size={this.state.dashboardIconSize}
              style={{
                marginRight: windowWidth/8,
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>this.gotoScreen(2)} style={{
            //backgroundColor: '#00ff00',
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            <Icon
              name='history'
              color={this.state.historyIconColor}              
              size={this.state.historyIconSize}
              style={{
                marginLeft: windowWidth/8,
              }}
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={()=>this.gotoScreen(0)} style={{ 
          position: 'absolute', 
          alignSelf: 'center', 
          // backgroundColor: '#0000ff',
          width: windowHeight/6, height: windowHeight/8, 
          bottom: windowHeight/40, 
          zIndex: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
          <View style={{ 
            position: 'absolute', 
            alignSelf: 'center', 
            backgroundColor: '#f8f4f4',
            width: windowHeight/10, height: windowHeight/10, 
            borderRadius: windowHeight/20, 
            zIndex: 10,          
            justifyContent: 'center',  
            alignItems: 'center',
            bottom: 0,
            
          }}>
            <Icon
              name={this.state.addIconName}
              type='material'
              color={this.state.addIconColor}
              size={this.state.addIconSize}              
            />
          </View>
        </TouchableOpacity>
        
      </View>
    );
  }
}

export default App;
